package main.java.utilities;

import org.aeonbits.owner.Config;

@Config.Sources("classpath:test.properties")
public interface Configuration extends Config {

    @DefaultValue("https://www.saucedemo.com/")
    @Key("common.data.system.url")
    String url();

    @DefaultValue("QA")
    @Key("common.data.system.environment")
    String environment();

    @DefaultValue("Chrome")
    @Key("common.data.system.browser")
    String browser();

    @Key("common.data.username")
    String user();

    @Key("common.data.password")
    String password();


    @Key("common.execution.grid.hub")
    String gridHub();

    @Key("common.data.system.implicitWait")
    int implicitWait();

}
