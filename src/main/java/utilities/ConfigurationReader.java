package main.java.utilities;


import org.aeonbits.owner.ConfigFactory;

public class ConfigurationReader {

    private static Configuration configuration;

    private ConfigurationReader() {
    }

    public static Configuration get() {
        if (configuration == null) {
            configuration = ConfigFactory.create(Configuration.class, System.getProperties());
        }
        return configuration;
    }

}
