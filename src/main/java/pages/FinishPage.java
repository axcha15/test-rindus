package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FinishPage extends PageObject{

    @FindBy(xpath = ".//*[text()='Finish']")
    private WebElement finishTitle;

    @FindBy(xpath = ".//*[text()='THANK YOU FOR YOUR ORDER']")
    private WebElement thankYou;

   public FinishPage() {

        super();
    }


    public boolean isInitialized()
    {
        Log.info("Attempt to validate if the final page is being displayed");
        return finishTitle.isDisplayed();
    }

    public String getThankYouMessage(){
        Log.info("Attempt to validate thank you message");
        return thankYou.getText();
    }
}
