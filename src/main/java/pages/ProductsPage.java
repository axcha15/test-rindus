package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductsPage extends PageObject {

    @FindBy(className = "product_label")
    private WebElement productsTitle;

    @FindBy(xpath = ".//*[@id='item_4_title_link']/div[@class='inventory_item_name']")
    private WebElement productName;

    @FindBy(xpath = ".//*[@class='inventory_item'][1]/div[@class='pricebar']/div[@class='inventory_item_price']")
    private WebElement productPrice;

    @FindBy(xpath = ".//*[1]/div[@class='inventory_item_label']/div[@class='inventory_item_desc']")
    private WebElement productDescription;

    @FindBy(xpath = ".//*[1]/div[@class='pricebar']/button[@class='btn_primary btn_inventory']")
    private WebElement addToCartProduct;

    @FindBy(xpath = "//a[@href='./cart.html']")
    private WebElement cart;



    public ProductsPage() {

        super();
    }

    public boolean isInitialized()
    {
        Log.info("Attempt to validate if the Product title is displayed");
        return productsTitle.isDisplayed();
    }

    public String getPrice(){
        Log.info("Attempt to validate product Price");
        return productPrice.getText();
    }

    public void addProductToCart(){
        Log.info("Attempt to add product to Cart");
        addToCartProduct.click();
    }

    public String getDescription(){
        Log.info("Attempt to validate product Description");
        return productDescription.getText();
    }

    public String getName(){
        Log.info("Attempt to validate product Name");
        return productName.getText();
    }

    public void gotToCart(){
        Log.info("Attempt to check cart products");
        cart.click();
    }

}