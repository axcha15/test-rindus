package main.java.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BasePage {

    protected final String ATTEMPT_TO_CLICK = "Attempt to click on %s";
    protected final String ATTEMPT_TO_GET_TEXT = "Attempt to get text from %s";
    protected final String ATTEMPT_TO_VALIDATE = "Attempt to validate %s";


    public void writeInField(WebElement element, String name){
        element.click();
        element.clear();
        element.sendKeys(name);
    }

    public void select(WebElement element,String text){
        Select dropdown = new Select(element);
        dropdown.selectByValue(text);
    }

    public void selectByText(WebElement element,String text){
        Select dropdown = new Select(element);
        dropdown.selectByVisibleText(text);
    }
}
