package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPage extends PageObject {

    @FindBy(id="user-name")
    private WebElement usernameField;

    @FindBy(id="password")
    private WebElement passwordField;

    @FindBy(id="login-button")
    private WebElement loginButton;

    public LoginPage() {
        super();
    }

    public boolean isInitialized() {
        return usernameField.isDisplayed();
    }

    public void typeUsername(String  username){
        Log.info(String.format("Attempt to login with user: %s", username));
        writeInField(usernameField, username);
    }

    public void typePassword(String  password){
        Log.info(String.format("Attempt to login with password: %s", password));
        writeInField(passwordField, password);
    }

    public void submit(){
        Log.info("Attempt to submit login");
        this.loginButton.submit();



    }
}
