package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageObject{

    @FindBy(className = "bm-burger-button")
    private WebElement hamburgerMenu;

    @FindBy(id = "logout_sidebar_link")
    private WebElement logout;

    public HomePage() {

        super();
    }


    public void clickHamburgerMenu(){
        Log.info("Attempt to click hamburger menu");
        hamburgerMenu.click();
    }

    public void clickLogout(){
        Log.info("Attempt to click logout button");
        logout.click();
    }
}
