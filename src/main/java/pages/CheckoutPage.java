package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckoutPage extends PageObject{

    @FindBy(id="first-name")
    private WebElement firstNameField;

    @FindBy(id="last-name")
    private WebElement lastNameField;

    @FindBy(id="postal-code")
    private WebElement zipField;

    @FindBy(xpath=".//*[@class='btn_primary cart_button']")
    private WebElement continueButton;

    public CheckoutPage() {
        super();
    }

    public boolean isInitialized() {
        return firstNameField.isDisplayed();
    }

    public void typeFirstName(String  firstName){
        Log.info(String.format("Attempt to type first name: %s", firstName));
        writeInField(firstNameField, firstName);
    }

    public void typeLastName(String  lastName){
        Log.info(String.format("Attempt to type last name: %s", lastName));
        writeInField(lastNameField, lastName);
    }

    public void typeZip(String  zipCode){
        Log.info(String.format("Attempt to type zip: %s", zipCode));
        writeInField(zipField, zipCode);
    }

    public void clickContinue(){
        Log.info(String.format("Attempt to click continue button", continueButton));
        continueButton.click();
    }
}
