package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends PageObject{

        @FindBy(xpath = ".//*[@class='btn_action checkout_button']")
        private WebElement checkoutButton;

        @FindBy(className = "cart_quantity")
        private WebElement productQuantity;

        @FindBy(className = "inventory_item_price")
        private WebElement productPrice;

        @FindBy(className = "inventory_item_name")
        private WebElement productName;

        @FindBy(className = "inventory_item_desc")
        private WebElement productDescription;

       public CartPage() {

            super();
        }


    public boolean isInitialized()
    {
        Log.info("Attempt to validate if the checkout page is displayed");
        return checkoutButton.isDisplayed();
    }

    public String getPrice(){
        Log.info("Attempt to validate product Price");
        return productPrice.getText();
    }

    public String getQuantity(){
        Log.info("Attempt to validate product Quantity");
        return productQuantity.getText();
    }

    public String getName(){
        Log.info("Attempt to validate product Name");
        return productName.getText();
    }

    public String getDescription(){
        Log.info("Attempt to validate product Description");
        return productDescription.getText();
    }

    public void clickCheckout(){
        Log.info("Attempt to proceed with checkout");
        checkoutButton.click();
    }
}
