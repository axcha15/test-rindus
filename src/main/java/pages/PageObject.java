package main.java.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import main.java.testController.DriverManager;
import org.openqa.selenium.support.ui.Select;


public class PageObject extends BasePage{
    protected final WebDriver driver;

    public PageObject(){

        this.driver = DriverManager.getDriver();
        PageFactory.initElements(driver, this);
    }


}