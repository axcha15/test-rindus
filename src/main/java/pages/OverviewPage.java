package main.java.pages;

import main.java.utilities.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OverviewPage  extends PageObject{

    @FindBy(xpath = ".//*[@class='btn_action cart_button']")
    private WebElement finishButton;

    @FindBy(className = "summary_quantity")
    private WebElement productQuantity;

    @FindBy(className = "inventory_item_price")
    private WebElement productPrice;

    @FindBy(className = "inventory_item_name")
    private WebElement productName;

    @FindBy(className = "inventory_item_desc")
    private WebElement productDescription;

    @FindBy(className = "summary_subtotal_label")
    private WebElement itemTotal;

    @FindBy(className = "summary_tax_label")
    private WebElement tax;

    @FindBy(className = "summary_total_label")
    private WebElement total;

    public OverviewPage() {

        super();
    }


    public boolean isInitialized()
    {
        Log.info("Attempt to validate if the checkout page is displayed");
        return finishButton.isDisplayed();
    }

    public String getPrice(){
        Log.info("Attempt to validate product Price");
        return productPrice.getText();
    }

    public String getQuantity(){
        Log.info("Attempt to validate product Quantity");
        return productQuantity.getText();
    }

    public String getName(){
        Log.info("Attempt to validate product Name");
        return productName.getText();
    }

    public String getDescription(){
        Log.info("Attempt to validate product Description");
        return productDescription.getText();
    }

    public void clickFinish(){
        Log.info("Attempt to finish checkout");
        finishButton.click();
    }

    public String getItemTotal(){
        Log.info("Attempt to validate item total");
        return itemTotal.getText();
    }

    public String getTax(){
        Log.info("Attempt to validate tax");
        return tax.getText();
    }

    public String getTotal(){
        Log.info("Attempt to validate order total");
        return total.getText();
    }

}
