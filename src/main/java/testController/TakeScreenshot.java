package main.java.testController;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TakeScreenshot {

    public String saveScreenshotAndGetLocation(WebDriver driver, String testName)
    {
        ReportConfiguration config = new ReportConfiguration();
        String dateName = new SimpleDateFormat(config.getDatetimePattern()).format(new Date());
        String destination = "";
        try {

            TakesScreenshot ts=(TakesScreenshot)driver;
            File source=ts.getScreenshotAs(OutputType.FILE);
            destination = String.format(config.getScreenshotsLocation(),System.getProperty(config.getUserDirectoryPath()),testName,dateName);
            FileUtils.copyFile(source, new File(destination));
            return String.format(config.getScreenshotsLocation(),"..",testName,dateName);

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Screenshot taken");
        return destination;

    }
}
