package main.java.testController;

public class ReportConfiguration {

    public ReportConfiguration() {
    }

    private final static String HOST_NAME_PARAMETER = "Host Name";
    private final static String HOST_NAME_VALUE = "Local Machine";
    private final static String ENVIRONMENT_PARAMETER = "Environment";
    private final static String USER_NAME_PARAMETER = "User Name";
    private final static String BROWSER_PARAMETER = "Browser";
    private final static String USER_NAME_VALUE = "Alex C";
    private final static String USER_DIRECTORY_PATH = "user.dir";
    private final static String EXTENT_CONFIG_PATH = "src\\main\\resources\\extent-config.xml";
    private static final String SCREENSHOTS_LOCATION = "%s\\Screenshots\\%s%s.png";
    private static final String EXECUTION_REPORT_LOCATION = "/Reports/AutomationReport.html";
    private static final String DATETIME_PATTERN = "yyyyMMddhhmmss";

    public String getDatetimePattern() {
        return DATETIME_PATTERN;
    }
    public String getScreenshotsLocation() {
        return SCREENSHOTS_LOCATION;
    }

    public String getExecutionReportLocation() {
        return EXECUTION_REPORT_LOCATION;
    }

    public String getHostNameParameter() {
        return HOST_NAME_PARAMETER;
    }

    public String getHostNameValue() {
        return HOST_NAME_VALUE;
    }

    public String getEnvironmentParameter() {
        return ENVIRONMENT_PARAMETER;
    }

    public String getUserNameParameter() {
        return USER_NAME_PARAMETER;
    }

    public String getUserNameValue() {
        return USER_NAME_VALUE;
    }

    public String getBrowserParameter() {
        return BROWSER_PARAMETER;
    }

    public String getUserDirectoryPath() {
        return USER_DIRECTORY_PATH;
    }

    public String getExtentConfigPath() {
        return EXTENT_CONFIG_PATH;
    }



}
