package main.java.testController;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import main.java.utilities.ConfigurationReader;
import org.testng.ITestResult;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("CanBeFinal")
public class ExtendReportClass {

    public enum TestResults
    {
        SUCCESS(ITestResult.SUCCESS){
            @Override
            void checkStatus(ITestResult result) {
                String TEST_PASSED = "Test case passed without errors";
                logger.log(Status.PASS, TEST_PASSED);
            }
        },
        FAILED(ITestResult.FAILURE){
            @Override
            void checkStatus(ITestResult result) {
                  updateReportWithFailingDetails(result);
            }
        },
        SKIPPED(ITestResult.SKIP){
            @Override
            void checkStatus(ITestResult result) {
                String TEST_SKIPPED = "Test case skipped is: %s";
                logger.log(Status.SKIP, String.format(TEST_SKIPPED, result.getName()));
            }
        };

        private final int status;

        TestResults(int status)
        {
            this.status = status;
        }

        abstract void checkStatus(ITestResult result);
    }


    private static ExtentReports extent = new ExtentReports();
    private static ExtentTest logger;


    public void setupReportEnvironment()
    {
        ReportConfiguration config = new ReportConfiguration();
        ExtentHtmlReporter htmlReporter= new ExtentHtmlReporter(System.getProperty(config.getUserDirectoryPath()) + config.getExecutionReportLocation());
        htmlReporter.loadXMLConfig(config.getExtentConfigPath());
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo(config.getHostNameParameter(), config.getHostNameValue());
        extent.setSystemInfo(config.getEnvironmentParameter(), ConfigurationReader.get().environment());
        extent.setSystemInfo(config.getUserNameParameter(), config.getUserNameValue());
        extent.setSystemInfo(config.getBrowserParameter(), ConfigurationReader.get().browser());
        extent.setReportUsesManualConfiguration(true);

    }

    private static void updateReportWithFailingDetails(ITestResult result)
    {
        try {
            String TEST_FAILING = "Test Case failing is: %s";
            String FAILURE_REASON = "Test case failed due to: %s";
            TakeScreenshot screenshot= new TakeScreenshot();
            String destination = screenshot.saveScreenshotAndGetLocation(DriverManager.getDriver(), result.getName());
            logger.log( Status.FAIL, String.format(TEST_FAILING, result.getName()));
            logger.log(Status.FAIL, String.format(FAILURE_REASON, result.getThrowable()));
            logger.log(Status.FAIL, "Snapshot below: " + logger.addScreenCaptureFromPath(destination));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void checkTestStatus(ITestResult result) {
        for(TestResults itemResult: TestResults.values())
        {
            if(result.getStatus() == itemResult.status)
            {
                itemResult.checkStatus(result);
            }
        }

        logger.getModel().setStartTime(getTime(result.getStartMillis()));
        logger.getModel().setEndTime(getTime(result.getEndMillis()));
    }

    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }

    public void startTest(String methodName) {
        logger = extent.createTest(methodName);
    }

    public void closeReports() {

        extent.flush();
    }


}
