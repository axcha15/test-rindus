package main.java.testController.drivers;

import org.openqa.selenium.WebDriver;

public interface BaseDriver{

    WebDriver startDriver();

}
