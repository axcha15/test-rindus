package main.java.testController.drivers;

import main.java.utilities.ConfigurationReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class ChromeGrid implements BaseDriver{

    public WebDriver startDriver(){
        ChromeOptions options = new ChromeOptions();
        WebDriver remoteDriver = null;
        try {
            remoteDriver = new RemoteWebDriver(new URL(ConfigurationReader.get().gridHub()), options);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return remoteDriver;

    }


}
