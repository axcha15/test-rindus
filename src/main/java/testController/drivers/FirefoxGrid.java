package main.java.testController.drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import main.java.utilities.ConfigurationReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class FirefoxGrid implements BaseDriver{

    public WebDriver startDriver(){
        FirefoxOptions options = setFirefoxOptions();
        WebDriver remoteDriver = null;
        try {
            remoteDriver = new RemoteWebDriver(new URL(ConfigurationReader.get().gridHub()), options);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return remoteDriver;

    }

    private static FirefoxOptions setFirefoxOptions()
    {
        FirefoxOptions options = new FirefoxOptions();
        options.setCapability("marionette", true);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.private.browsing.autostart",true);
        options.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
        return options;

    }
}
