package main.java.testController.drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class Firefox implements BaseDriver{

    public WebDriver startDriver(){
        WebDriverManager.firefoxdriver().setup();
        return  new FirefoxDriver(setFirefoxOptions());
    }

    private static FirefoxOptions setFirefoxOptions()
    {
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.private.browsing.autostart",true);
        options.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
        return options;

    }

}
