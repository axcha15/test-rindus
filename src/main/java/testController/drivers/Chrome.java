package main.java.testController.drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Chrome implements BaseDriver{

    public WebDriver startDriver(){
        WebDriverManager.chromedriver().setup();
        return  new ChromeDriver(setChromeOptions());
    }

    private static ChromeOptions setChromeOptions()
    {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        return options;
    }

}
