package main.java.testController;

import main.java.utilities.Log;

public class LoggerManager {


    public void initLogs(String testName){
        Log.startTestCase(testName);
    }

    public void endLogs(String description) {
        Log.endTestCase(description);

    }
}
