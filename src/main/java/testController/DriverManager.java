package main.java.testController;

import main.java.testController.drivers.*;
import main.java.utilities.ConfigurationReader;
import main.java.utilities.Log;
import org.openqa.selenium.WebDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;


public class DriverManager {

    public enum Browsers {CHROME,FIREFOX,CHROME_GRID,FIREFOX_GRID}

    @SuppressWarnings("CanBeFinal")
    private static ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();

    private DriverManager(){}

    public static WebDriver getDriver() {
        return threadLocalDriver.get();
    }

    public static void initSession() {
        setDriver(ConfigurationReader.get().browser());
        getDriver().manage().timeouts().implicitlyWait(ConfigurationReader.get().implicitWait(), TimeUnit.SECONDS);
        openURL(getDriver());
    }

    public static void openURL(WebDriver driver )
    {
        driver.manage().window().maximize();
        String url = ConfigurationReader.get().url();
        driver.get(url);
        Log.info("Page has been opened :" + url);
    }

    public static void setDriver(String selectedBrowser)
    {
        BaseDriver driverInstance = null;
        if(selectedBrowser.equalsIgnoreCase(Browsers.CHROME.toString()))
        {
            driverInstance = new Chrome();
        }
        else if(selectedBrowser.equalsIgnoreCase(Browsers.FIREFOX.toString()))
        {
            driverInstance = new Firefox();
        }
        else if(selectedBrowser.equalsIgnoreCase(Browsers.CHROME_GRID.toString()))
        {
            driverInstance = new ChromeGrid();
        }
        else if(selectedBrowser.equalsIgnoreCase(Browsers.FIREFOX_GRID.toString()))
        {
            driverInstance = new FirefoxGrid();
        }
        threadLocalDriver.set(Objects.requireNonNull(driverInstance).startDriver());
        Log.info(String.format("Driver of type: %s has been created", ConfigurationReader.get().browser()));

    }

    public static void closeSession() {
        getDriver().manage().deleteAllCookies();
        getDriver().close();
        //getDriver().quit();
        Log.info("Driver is closed");

    }

    public static void quitSession() {
         threadLocalDriver.remove();

    }






}
