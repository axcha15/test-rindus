Feature: Test checkout process

  Scenario: successful checkout
    Given Given user login with correct "standard_user" and "secret_sauce"
    And the user adds a product to the cart
    And the user checks the cart products
    And the user proceeds to checkout
    And the user enters "Alex" as first name, "Chaves" as last name and "21050" as zip code
    Then the user validates the tax as "$2.40" and total as "$32.39"
    And the user confirms the purchase
    Then the user validates that the purchase has been completed successfully
    And the user logs out and validates that the session has been closed


  Scenario: Login with invalid user
    Given Given user login with correct "fake" and "fake"


