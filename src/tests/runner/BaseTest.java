package tests.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import main.java.testController.DriverManager;
import main.java.testController.ExtendReportClass;
import main.java.testController.LoggerManager;
import org.testng.ITestResult;
import org.testng.annotations.*;

@CucumberOptions(features = "src/tests/resource", glue = "tests.steps")

public class BaseTest extends AbstractTestNGCucumberTests {

    private final ExtendReportClass reports = new ExtendReportClass();
    private static final LoggerManager loggerManager = new LoggerManager();



    @BeforeTest
    public void startEnvironment()
    {
        reports.setupReportEnvironment();
    }

    @BeforeMethod
    public void setUp(ITestResult result)
    {
        DriverManager.initSession();
        reports.startTest(result.getMethod().getMethodName());
        loggerManager.initLogs(result.getMethod().getDescription());

    }

    @AfterMethod
    public  void tearDown(ITestResult result)
    {
        reports.checkTestStatus(result);
        DriverManager.closeSession();
        loggerManager.endLogs(result.getMethod().getDescription());

    }

    @AfterTest
    public void closeEnvironment()
    {
        reports.closeReports();
    }

    @AfterSuite
    public void close(){

        DriverManager.quitSession();

    }



}