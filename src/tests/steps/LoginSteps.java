package tests.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import main.java.pages.HomePage;
import main.java.pages.ProductsPage;
import main.java.pages.LoginPage;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;



public class LoginSteps {

    @Given("Given user login with correct {string} and {string}")
    public void the_user_has_navigated_to_the_homepage_and_will_login_with_and(String username, String password) {
        LoginPage loginPage = new LoginPage();
        Assert.assertTrue(loginPage.isInitialized());
        loginPage.typeUsername(username);
        loginPage.typePassword(password);
        loginPage.submit();
        ProductsPage products  = new ProductsPage();
        Assert.assertTrue(products.isInitialized());
    }

    @And("the user logs out and validates that the session has been closed")
    public void logout(){
        HomePage homePage = new HomePage();
        homePage.clickHamburgerMenu();
        homePage.clickLogout();
        LoginPage loginPage = new LoginPage();
        Assert.assertTrue(loginPage.isInitialized());
    }




}
