package tests.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import main.java.pages.*;
import main.java.saucedemo.Product;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class CheckoutSteps {

    private ProductsPage productsPage = new ProductsPage();
    private CartPage cartPage = new CartPage();
    private CheckoutPage checkoutPage= new CheckoutPage();
    private OverviewPage overviewPage= new OverviewPage();
    private FinishPage  finishPage = new FinishPage();
    private final static String QUANTITY_ONE = "1";
    private Product product;

    @And("the user adds a product to the cart")
    public void addProductToCart() {
        product = new Product(productsPage.getName(),productsPage.getPrice(),QUANTITY_ONE,
                productsPage.getDescription());
        productsPage.addProductToCart();

    }

    @And("the user checks the cart products")
    public void validateCartContents(){
        productsPage.gotToCart();
        SoftAssert assertion = new SoftAssert();
        assertion.assertEquals(product.getDescription(), cartPage.getDescription());
        assertion.assertEquals(product.getName(), cartPage.getName());
        /* @todo: workaround since there is a bug and the amount displayed is missing the $ symbol */
        assertion.assertEquals(product.getPrice().replace("$",""), cartPage.getPrice());
        assertion.assertEquals(product.getQuantity(), cartPage.getQuantity());
        assertion.assertAll();

    }

    @And("the user proceeds to checkout")
    public void goTocheckout(){
        cartPage.clickCheckout();
    }

    @And("the user enters {string} as first name, {string} as last name and {string} as zip code")
    public void checkingOut(String firstname, String lastName, String zip){
        Assert.assertTrue(checkoutPage.isInitialized());
        checkoutPage.typeFirstName(firstname);
        checkoutPage.typeLastName(lastName);
        checkoutPage.typeZip(zip);
        checkoutPage.clickContinue();

    }

    @Then("the user validates the tax as {string} and total as {string}")
    public void validateOverviewPage(String tax, String total){
        SoftAssert assertion = new SoftAssert();
        assertion.assertTrue(overviewPage.isInitialized());
        assertion.assertEquals(product.getDescription(), overviewPage.getDescription());
        assertion.assertEquals(product.getName(), overviewPage.getName());
        assertion.assertEquals(product.getPrice(), overviewPage.getPrice());
        assertion.assertEquals(product.getQuantity(), overviewPage.getQuantity());
        assertion.assertTrue(overviewPage.getItemTotal().contains(product.getPrice()));
        assertion.assertTrue(overviewPage.getTax().contains(tax));
        assertion.assertTrue(overviewPage.getTotal().contains(total));
        assertion.assertAll();
    }

    @And("the user confirms the purchase")
    public void finishPurchase(){
        overviewPage.clickFinish();
    }

    @Then("the user validates that the purchase has been completed successfully")
    public void validateFinishPage(){
        String THANK_YOU_MESSAGE = "THANK YOU FOR YOUR ORDER";
        SoftAssert assertion = new SoftAssert();
        assertion.assertTrue(finishPage.isInitialized());
        assertion.assertEquals(THANK_YOU_MESSAGE, finishPage.getThankYouMessage());
        assertion.assertAll();
    }


}
