In order to enable the grid follow these steps:

1-Download the latest version of selenium-server-standalone.jar rename it to match the following name "selenium-server-standalone.jar"(basically remove the version from the name) 
and place it in the same folder of the file StartGrid.bat.
2-Download the latest version of ChromeDriver and any other driver that you want to use, place in a folder in the computer(suggestion create a folder on C:/Drivers) 
3-Edit startGrid.bat file with the location of the drivers
4.Execute the StartGrid.bat file.
5-Configure the grid browser on the test.properties file, it could be "Chrome_grid" or "Firefox_grid"